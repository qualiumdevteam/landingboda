<?php
/**
 * Boda functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Boda
 */

if ( ! function_exists( 'boda_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function boda_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Boda, use a find and replace
	 * to change 'boda' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'boda', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'boda' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'boda_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'boda_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function boda_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'boda_content_width', 640 );
}
add_action( 'after_setup_theme', 'boda_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function boda_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'boda' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'boda' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'boda_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function boda_scripts() {
	wp_enqueue_style( 'boda-style', get_stylesheet_uri() );
	wp_enqueue_style( 'boda-foundation', get_template_directory_uri() . '/css/foundation.css');
	wp_enqueue_style( 'boda-owlcss', get_template_directory_uri() . '/css/owl.carousel.css');
	wp_enqueue_style( 'boda-owltheme', get_template_directory_uri() . '/css/owl.theme.default.css');
	wp_enqueue_style( 'boda-loader', get_template_directory_uri() . '/css/loader.css');
	wp_enqueue_style( 'boda-icon', get_template_directory_uri() . '/icon/flaticon.css');

	wp_enqueue_script( 'boda-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'boda-main', get_template_directory_uri() . '/js/main.js', array('jquery'), '20151215', true );
	wp_enqueue_script( 'boda-owl', get_template_directory_uri() . '/js/owl.carousel.min.js', array(), '20151215', true );
	wp_enqueue_script( 'boda-lodash', get_template_directory_uri() . '/js/lodash.min.js', array(), '20151215', true );
	wp_enqueue_script( 'boda-paginate', get_template_directory_uri() . '/js/paginate.js', array(), '20151215', true );
	wp_enqueue_script( 'boda-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'boda_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

require_once get_template_directory() . '/inc/funciones_ajax.php';

require_once get_template_directory() . '/inc/postype.php';

require_once get_template_directory() . '/inc/metabox.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
