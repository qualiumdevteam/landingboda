var data = jQuery('.item_recuerdo');
var per_page = 6;
var current_page = 1;

function gotoPage(page) {
    jQuery('#spinner_recuerdos').fadeOut();
    jQuery('#cortina_recuerdos').fadeOut();
    var dat = _.take(_.rest(data, (page - 1) * per_page), per_page);
    var hearts = jQuery('.heart');
    if(localStorage.getItem("likesrecuerdo")){
        var likesrecuerdo=JSON.parse(localStorage.getItem("likesrecuerdo"));
    }
    var articulos='';
    for (var i = 0; i < dat.length; i++) {
        articulos+='<div class="item_recuerdo">'+dat[i].innerHTML+'</div>';
     }
    jQuery('.caja').html(articulos);

        for (var e=0; e < likesrecuerdo.length; e++) {
            var resultado=jQuery('.heart[data-idrecuerdo='+likesrecuerdo[e]+']');
            console.log(resultado);
            if(resultado!=''){
                var selector1=jQuery('.heart[data-idrecuerdo='+likesrecuerdo[e]+']').children('.heartvacio');
                var selector2=jQuery('.heart[data-idrecuerdo='+likesrecuerdo[e]+']').children('.heartlleno');
                jQuery(selector1).removeClass('visible');
                jQuery(selector1).addClass('invisible');
                jQuery(selector2).removeClass('invisible');
                jQuery(selector2).addClass('visible');
            }
    }
}

jQuery(function() {
    gotoPage(1);
    jQuery('.prev').click(function() {
        if (current_page > 1) {
            jQuery('#spinner_recuerdos').fadeIn();
            jQuery('#cortina_recuerdos').fadeIn();
            current_page -= 1;
            setTimeout(function(){
                gotoPage(current_page);
            }, 1000);
        }
    });


    jQuery('.next').click(function() {
        if (current_page < data.length / per_page) {
            current_page += 1;
                jQuery('#spinner_recuerdos').fadeIn();
                jQuery('#cortina_recuerdos').fadeIn();
                setTimeout(function () {
                    gotoPage(current_page);
                }, 1000);
            //setTimeout(gotoPage(current_page), 3000);
        }
    });
});