jQuery(document).ready(function(){

    var owlvar= jQuery('.content_carosel');

    jQuery('.novia').addClass('resaltar');
    jQuery('.novio').addClass('resaltar');
    jQuery('.logo').addClass('visible');
    jQuery('.fecha').css('bottom','5%');

    jQuery( ".content_recuerdos" ).on("click",'.glyph-icon', function() {
        var idmensaje=jQuery(this).parent().data('idrecuerdo');
        var selector= jQuery(this).parent();

        if(localStorage.getItem("likesrecuerdo")){
            var likesrecuerdo=JSON.parse(localStorage.getItem("likesrecuerdo"));
        }
        else{
            var likesrecuerdo=[];
        }

        if(likesrecuerdo.indexOf(idmensaje)==-1) {
            jQuery.ajax({
                url: '/wp-admin/admin-ajax.php',
                timeout: 8000,
                dataType: 'json',
                type: 'POST',
                data: {
                    action: 'add_likemensaje',
                    idmensaje: idmensaje
                },
                beforeSend: function () {
                    jQuery('#cortina_mensaje').fadeIn();
                    jQuery('#loaderglobal').fadeIn();
                },
                success: function (data) {
                    jQuery('#cortina_mensaje').fadeOut();
                    jQuery('#loaderglobal').fadeOut();
                    jQuery(selector).find('.likes').text(data);
                    jQuery(selector).find('.heartvacio').removeClass('visible');
                    jQuery(selector).find('.heartvacio').addClass('invisible');
                    jQuery(selector).find('.heartlleno').removeClass('invisible');
                    jQuery(selector).find('.heartlleno').addClass('visible');
                    if (!localStorage.getItem("likesrecuerdo")) {
                        localStorage.setItem("likesrecuerdo", JSON.stringify([idmensaje]));
                    }
                    else {
                        var likesrecuerdo = JSON.parse(localStorage.getItem("likesrecuerdo"));
                        likesrecuerdo.push(idmensaje);
                        localStorage.setItem("likesrecuerdo", JSON.stringify(likesrecuerdo));
                    }
                    console.log(JSON.parse(localStorage.getItem("likesrecuerdo")));
                },
                error: function (err) {

                }
            })
        }
    });
    var ancho=jQuery(window).width();

    if(ancho>640) {
        var alto = jQuery(window).height();
        jQuery(window).scroll(function () {
            var scrolltop = jQuery(".home").scrollTop();
            if (scrolltop > jQuery('.portada').position().top) {
                jQuery('.content_ramo').css({'-webkit-transform': 'translatey(-' + scrolltop / 20 + '%)','transform': 'translatey(-' + scrolltop / 20 + '%)','-moz-transform': 'translatey(-' + scrolltop / 20 + '%)'});
                jQuery('.letraup').css({'-webkit-transform': 'translatey(-' + scrolltop / 5 + '%)','transform': 'translatey(-' + scrolltop / 5 + '%)','-moz-transform': 'translatey(-' + scrolltop / 5 + '%)'});
                jQuery('.letradown').css({'-webkit-transform': 'translatey(' + scrolltop / 5 + '%)','transform': 'translatey(' + scrolltop / 5 + '%)','-moz-transform': 'translatey(' + scrolltop / 5 + '%)'});
            }
            if(scrolltop > jQuery('.bienvenido').position().top){
                jQuery('.content_local').css({'-webkit-transform': 'translatey(-' + scrolltop / 20 + '%)','transform': 'translatey(-' + scrolltop / 20 + '%)','-moz-transform': 'translatey(-' + scrolltop / 20 + '%)'});
            }
        })
    }

    jQuery('.enviar_mensaje').click(function(){

        var nombre= jQuery('.nombre').val();
        var mensaje= jQuery('.mensaje').val();
        var update_mensaje='';

        if(nombre && mensaje !='') {
            jQuery.ajax({
                url: '/wp-admin/admin-ajax.php',
                timeout: 8000,
                dataType: 'json',
                type: 'POST',
                data: {
                    action: 'add_mensaje',
                    nombre: nombre,
                    mensaje: mensaje
                },
                beforeSend: function () {
                    jQuery('#loadermensaje').fadeIn();
                },
                success: function (data) {
                    jQuery('#loadermensaje').fadeOut();
                    jQuery('.nombre').val('');
                    jQuery('.mensaje').val('');
                    owlvar.trigger('destroy.owl.carousel');
                    for (var i=0; i < data.length; i++){
                        update_mensaje+='<div class="item"><p>'+data[i].mensaje.substr(0,100)+'</p><p class="persona">'+data[i].persona+' |<span>'+data[i].fecha+'</span></p></div>'
                    }
                    jQuery('.content_carosel').html(update_mensaje);
                    owlvar.owlCarousel({
                        loop:false,
                        margin:50,
                        dots: false,
                        nav: true,
                        responsive:{
                            0:{
                                items:1,
                                margin:0
                            },
                            600:{
                                items:2
                            },

                            1040:{
                                items:2
                            },

                            1200:{
                                items:4
                            }
                        }
                    })
                    /*if(data==1){
                        jQuery('.respuesta').text('Gracias por tu mensaje');
                    }else{
                        jQuery('.respuesta').text('algo salio mal intenta de nuevo');
                    }*/
                },
                error: function (err) {

                }
            })
        }
        else{
            alert('llena todos los campos')
        }
    })

    jQuery('.content_carosel').on('click','.item',function(){
        var id= jQuery(this).data('id');
        var parentitem = this;
        //jQuery(this).children('.spinner').fadeIn();
        jQuery.ajax({
            url: '/wp-admin/admin-ajax.php',
            timeout: 8000,
            dataType: 'json',
            type: 'POST',
            data: {
                action: 'get_mensaje',
                id: id
            },
            beforeSend: function () {
               jQuery(parentitem).children('.spinner').fadeIn();
            },
            success: function (data) {
                jQuery('.spinner').fadeOut();
                jQuery('#cortina_mensaje').fadeIn();
                jQuery('.showmensaje').fadeIn();
                jQuery('.showmensaje').find('.contenido').html('<p>'+data[0].mensaje+'</p><p class="persona">'+data[0].persona+' |<span>'+data[0].fecha+'</span></p>');
            },
            error: function (err) {

            }
        })

    })

    jQuery('.closemensaje').click(function(){
        jQuery('#cortina_mensaje').fadeOut();
        jQuery('.showmensaje').fadeOut();
    })


    owlvar.owlCarousel({
        loop:false,
        margin:50,
        dots: false,
        nav: true,
        responsive:{
            0:{
                items:1,
                margin:0
            },
            600:{
                items:2
            },

            1040:{
                items:2
            },

            1200:{
                items:4
            }
        }
    })
})