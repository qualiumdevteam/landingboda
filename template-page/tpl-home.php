<?php
/*
* Template Name: Home
*/
get_header();

$args = array(
    'post_type' => 'mensajes',
    'posts_per_page' => -1,
    'order' => 'DESC'
);

$args_recuerdos = array(
    'post_type' => 'recuerdos',
    'posts_per_page' => -1,
    'order' => 'ASC'
);
?>
<div class="home">
    <div class="icons">
        <img class="corazon" src="<?php echo get_template_directory_uri() ?>/img/icon/corazon.svg">
        <img class="flor" src="<?php echo get_template_directory_uri() ?>/img/icon/flor.svg">
        <img class="mono" src="<?php echo get_template_directory_uri() ?>/img/icon/mono.svg">
        <img class="zapato" src="<?php echo get_template_directory_uri() ?>/img/icon/zapato.svg">
        <img class="mono2" src="<?php echo get_template_directory_uri() ?>/img/icon/mono.svg">
    </div>
    <section class="portada">
        <div class="text-center novia">Fernanda Romero Gamboa</div>
        <div class="logo"><img src="<?php echo get_template_directory_uri() ?>/img/logo.png"></div>
        <div class="text-center novio">Alejandro Bermea Elizondo</div>
        <div class="fecha"><p>21.01.2017</p></div>
    </section>
    <section class="bienvenido">
        <div class="cuadrado"></div>
        <img class="ene letraup" src="<?php echo get_template_directory_uri() ?>/img/icon/ene.svg">
        <div class="small-12 medium-6 large-6 columns content_bienvenida">
            <div class="texto">
                <h1 class="titulo">Bienvenido</h1>
                <p>Esta no es una página cualqiuera, esta página es un símbolo del amor entre dos personas, Nani y Alejandro, los cuales unificarán sus vidas el <span>21 de enero del 2017</span>, si estás leyendo esto y sabes quienes somos, estás invitado a la celebración más importante de nuestras vidas, y queremos que estés ahi...</p>
            </div>
        </div>
        <div class="small-12 medium-6 large-6 columns imagenes">
            <div class="content_novia"><div class="novia"></div></div>
            <div class="content_ramo"><div class="ramo"></div></div>
        </div>
    </section>
    <section class="celebremos">
        <div class="cuadrado"></div>
        <img class="letraa letraup" src="<?php echo get_template_directory_uri() ?>/img/icon/a.svg">
        <div class="small-12 medium-6 large-6 columns imagenes">
            <div class="content_mesa"><div class="mesa"></div></div>
            <div class="content_local"><div class="local"></div></div>
        </div>
        <div class="small-12 medium-6 large-6 columns content_celebremos">
            <div class="texto">
                <h1 class="titulo">Celebremos</h1>
                <p>La celebración comenzará con una misa, la cual se llevará a cabo en Nuestra Señora de Líbano a las 20:30 hrs, posteriormente los esperamos en la recepción en la Quinta Alsina Itzimná a partir de las 21:30 hrs.</p>
            </div>
        </div>
    </section>
    <section class="recuerdas">
        <img class="ene letradown" src="<?php echo get_template_directory_uri() ?>/img/icon/ene.svg">
        <h1 class="titulo">Recuerdas</h1>
        <div class="texto"><p>¿Recuerdas todos esos momentos juntos? Nosotros sí, y es por eso que queremos que estés en este día tan especial que marcará el resto de nuestras vidas. Gracias por ser parte de ellas. Ahora podrás ver algunos momentos que hemos pasado juntos, ¡encuéntrate!</p></div>
        <div class="content_recuerdos">
            <div id="cortina_recuerdos" class="reveal-overlay"></div>
            <div style="height: 100%" class="caja">
        <?php $query_recuerdos = new WP_Query($args_recuerdos);
        while ( $query_recuerdos->have_posts() ) : $query_recuerdos->the_post();
            $feat_image_recuerdos = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
            $numlikes=get_post_meta(get_the_ID(),'conteolikes',true);
            if($numlikes==''){
                $likestotal=0;
            }else{
                $likestotal=$numlikes;
            }
            ?>
            <div class="item_recuerdo">
                <div style="background-image: url('<?php echo $feat_image_recuerdos; ?>')" class="imagengal">
                    <div data-idrecuerdo="<?php echo get_the_ID(); ?>" class="heart">
                        <div class="glyph-icon flaticon-favorite heartvacio visible"></div>
                        <div class="glyph-icon flaticon-favorite-1 heartlleno invisible"></div>
                        <span class="likes"><?php echo $likestotal; ?></span>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
            <?php wp_reset_query() ?>
            </div>
        </div>
        <div class="next"><img src="<?php echo get_template_directory_uri() ?>/img/navright.svg"></div>
        <div class="prev"><img src="<?php echo get_template_directory_uri() ?>/img/navleft.svg"></div>
        <div id="spinner_recuerdos" class='spinner'></div>
    </section>
    <section class="dinosalgo">
        <img class="letraa letraup" src="<?php echo get_template_directory_uri() ?>/img/icon/a.svg">
        <div class="small-12 medium-6 large-6 columns">
            <h1 class="titulo">Dinos algo</h1>
            <div class="texto"><p>¿Quieres decir algo? ¡Nos encantaría! Tus deseos y opiniones son muy importantes para nosotros, es por eso que aquí puedes dejar tu mensaje y se publicará junto al de los demás dentro de la página, así todos lo podremos ver.</p></div>
        </div>
        <div class="small-12 medium-6 large-6 columns form_mensaje">
            <input name="nombre" class="nombre" value="" placeholder="Nombre">
            <textarea name="mensaje" class="mensaje" placeholder="¿Que quieres decirnos?" rows="5"></textarea>
            <input class="enviar_mensaje" type="button" value="Enviar">
            <p class="respuesta"></p>
            <div id="loadermensaje" class='spinner'></div>
        </div>
        <div class="clearfix"></div>
        <div class="content_carosel">
            <?php $query = new WP_Query($args);
            while ( $query->have_posts() ) : $query->the_post(); ?>
            <div data-id="<?php echo get_the_ID(); ?>" class="item">
                <div id="spinner_mensajes" class='spinner'></div>
                <p><?php echo substr(get_the_content(),0,100); ?>...</p>
                <p class="persona"><?php echo get_the_title(); ?> |<span><?php echo get_the_date() ?></span></p>
            </div>
            <?php endwhile; ?>
        </div>
    </section>
    <section class="contacto">
        <div class="cuadrado"></div>
        <div class="medium-6 large-6 columns form_contacto">
            <div class="center">
                <h1 class="titulo">Confirma tu <br> asistencia</h1>
                <p>Es muy importante para nosotros saber con quienes contaremos en este día tan especial, por lo que agradeceríamos que confirmaras tu asistencia al evento. ¡Gracias!</p>
                <div class="form">
                    <!--<input type="text" name="nombrecontacto" placeholder="Nombre">
                    <input type="text" name="telefonocontacto" placeholder="Teléfono">
                    <input type="text" name="correo" placeholder="Correo Electrónico">
                    <input type="button" class="asistir" value="Asistire">-->
                    <?php echo do_shortcode('[contact-form-7 id="25" title="Sin título"]'); ?>

                </div>
            </div>
        </div>
        <div class="medium-6 large-6 column mapa">
            <div class="center">
                <p class="lugar">QUINTA ALSINA</p>
                <div style="background-image: url('<?php echo get_template_directory_uri() ?>/img/mapa.svg')" class="imgmapa">
                </div>
                <p class="direccion">C. 18 No. 106 por 21 y 21a Col. Itzimná</p>
            </div>
        </div>
    </section>
    <div id="cortina_mensaje" class="reveal-overlay"></div>
    <div id="loaderglobal" class='spinner'></div>
    <div class="showmensaje">
        <a class="closemensaje" href="javascript:void(0);"><img src="<?php echo get_template_directory_uri() ?>/img/X.svg"> </a>
        <div class="contenido">

        </div>
    </div>
</div>
<?php get_footer(); ?>
