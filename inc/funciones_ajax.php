<?php

add_action('wp_ajax_nopriv_add_mensaje', 'add_mensaje');
add_action('wp_ajax_add_mensaje', 'add_mensaje');
add_action('wp_ajax_nopriv_get_mensaje', 'get_mensaje');
add_action('wp_ajax_get_mensaje', 'get_mensaje');
add_action('wp_ajax_nopriv_add_likemensaje', 'add_likemensaje');
add_action('wp_ajax_add_likemensaje', 'add_likemensaje');

function add_likemensaje (){
    $idmensaje= $_POST['idmensaje'];

    $numlikes=get_post_meta($idmensaje,'conteolikes',true);

    update_post_meta($idmensaje,'conteolikes',$numlikes+1);

    echo get_post_meta($idmensaje,'conteolikes',true);

    exit();
}

function add_mensaje(){
    $nombre= $_POST['nombre'];
    $mensaje=$_POST['mensaje'];
    $post_mensajes=array();

    $mensajeinsert = array(
        'post_title'    => $nombre,
        'post_type'     => 'mensajes',
        'post_content'  => $mensaje,
        'post_status'   => 'publish',
        'post_author'   => 1,
        'post_category' => array(),
    );

    $mensaje_id=wp_insert_post($mensajeinsert);

    $args = array(
        'post_type' => 'mensajes',
        'posts_per_page' => -1,
        'order' => 'DESC'
    );

    $query = new WP_Query($args);
            while ( $query->have_posts() ) : $query->the_post();
                array_push($post_mensajes,array('mensaje'=> get_the_content(),'persona'=>get_the_title(),'fecha' => get_the_date()));
            endwhile;

        echo json_encode($post_mensajes);

    exit();
}

function get_mensaje (){
    $id= $_POST['id'];
    $get_mensajes=array();

    $args_mensajes = array(
        'post_type' => 'mensajes',
        'posts_per_page' => -1,
        'p' => $id
    );

    $query_mensajes = new WP_Query($args_mensajes);
    while ( $query_mensajes->have_posts() ) : $query_mensajes->the_post();
        array_push($get_mensajes,array('mensaje'=> get_the_content(),'persona'=>get_the_title(),'fecha' => get_the_date()));
    endwhile;

    echo json_encode($get_mensajes);

    exit();
}