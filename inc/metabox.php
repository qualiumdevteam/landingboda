<?php
add_action('add_meta_boxes', 'cyb_meta_boxes');
function cyb_meta_boxes()
{
    $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post'];
    $template_file = get_post_meta($post_id, '_wp_page_template', TRUE);
    $postname=get_post_type($post_id);
    if ($postname == 'recuerdos') {
        add_meta_box('cyb-meta-like', __('Conteo de likes'), 'cyb_meta_like', 'recuerdos');
    }
}

function cyb_meta_like(){
    global $post;
    $conteolike = get_post_meta($post->ID,'conteolikes',true);
    if($conteolike>0 || $conteolike=''){
        $numerolikes=$conteolike;
    }
    else{
        $numerolikes=0;
    }
    echo '<span class="dashicons dashicons-heart"></span><span style="margin-left: 10px">'.$numerolikes.'</span>';
}