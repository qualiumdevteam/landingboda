<?php
add_action( 'init', 'create_post_type' );
function create_post_type()
{
    register_post_type('Mensajes',
        array(
            'labels' => array(
                'name' => __('mensajes'),
                'singular_name' => __('mensaje')
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies' => array(),
            'supports' => array('title','editor')
        )
    );

    register_post_type('Recuerdos',
        array(
            'labels' => array(
                'name' => __('recuerdos'),
                'singular_name' => __('recuerdo')
            ),
            'public' => true,
            'has_archive' => true,
            'taxonomies' => array(),
            'supports' => array('title','thumbnail')
        )
    );
}